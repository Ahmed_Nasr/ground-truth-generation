import json
import numpy as np
import pandas as pd
import cv2

def more_points (x_points,y_points):
    y_all = []
    x_all = []

    for i in range (len(x_points)-1):
        if x_points[i]-x_points[i+1] ==0 : 
            y_new = np.linspace (y_points[i],y_points[i+1],num =9)
            y_all.append(y_new)
            x_new = np.ones_like(y_new)*x_points[i]
            x_all.append(x_new)
        else:
            m = (y_points[i]-y_points[i+1])/(x_points[i]-x_points[i+1])
            b = y_points[i]-(m*x_points[i])
            
            x_new = np.linspace (x_points[i],x_points[i+1],num=9,endpoint=False)
            y_new = m*x_new+b
            x_all.append(x_new)
            y_all.append(y_new)

    x_all = np.asarray (x_all)
    m_x,n_x = x_all.shape
    x_all = np.reshape(x_all,(m_x*n_x))
    x_all = x_all.tolist()
    x_all = [round(num_x) for num_x in x_all]
    
    y_all = np.asarray (y_all)
    m_y,n_y = y_all.shape
    y_all = np.reshape (y_all,(m_y*n_y))
    y_all = y_all.tolist()
    y_all = [round(num_y) for num_y in y_all]

    return x_all,y_all

train_data = []
df = pd.read_json('all_new.json')
df = df.T
df_data = pd.DataFrame(columns=['name', 'x_points','y_points'])

for index, row in df.iterrows():
    
    region =row['regions']
    filename = row['filename']
    filename = 'test_images/{}'.format(filename)
    if filename:
        if region:
            x_allall =[]
            y_allall=[]
            for reg in region:
                x_Values = reg['shape_attributes']['all_points_x']
                y_Values = reg['shape_attributes']['all_points_y']
                x_all ,y_all = more_points (x_Values,y_Values)
                for i in reg['shape_attributes']:
                    x_allall.append(x_all)
                    y_allall.append(y_all)
            data ={'lanes':x_allall , 'h_samples':y_allall,'raw_file':filename}
            with open("sample.json", "a") as outfile: 
                json.dump(data, outfile) 
                outfile.write('\n')



                
        



